import React from 'react';
import { Jumbotron, Button, Row, Col } from 'react-bootstrap';

export default function Banner() {
	return (
		<Row>
			<Col>
				<Jumbotron>
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for everyone, everyone</p>
					<Button className="bg-primary">Enroll Now!</Button>
				</Jumbotron>
			</Col>
		</Row>
	)
}