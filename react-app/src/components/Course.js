import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Button } from 'react-bootstrap';

export default function Course({ course }) {
	//console.log(course);
	const { name, description, price } = course;

	// [ getters, setters ]
	// count = 0;
	const [count, setCount] = useState(0);
	// console.log(useState(0)); // [count=0, setCount=f]
	// console.log(count);
	// setCount(100);
	// console.log(count);

	function enroll() {
		setCount(count + 1);
		console.log('Enrollees: ' + count);
	}

	if (course) {
		return (
			<Card>
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Text>
						<span className="subtitle">Description: </span>
						{description}
						<br />
						<span className="subtitle">Price: </span>
						PhP {price}
						<br />
					</Card.Text>
					<Card.Text>Enrollees: {count}</Card.Text>
					<Button variant="primary" onClick={enroll}>
						Enroll
					</Button>
				</Card.Body>
			</Card>
		);
	} else {
		return "";
	}
}

// Checks if the Course component is getting the correct prop structure/data type
Course.propTypes = {
	// shape() used to check if a prop object conforms or is the same to a specific "shape"/data structure/type
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

/*total = "45000" + 50000
total = "4500050000"

coursesData.foreach((course)=> {
	total = total + course.price
})*/

// Course {
// 	props: {
// 		key: "wdc001",
// 		course: {
// 			id: "wdc001",
// 			name: "PHP-Laravel",
// 			description: "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor",
// 			price: 45000,
// 			onOffer: true
// 		}
// 	},
// 	propTypes: {

// 	}
// }

// object destructuring
// { course }

// props.course.name
// props.course.description
// props.course.price

// const variable = "arvin";
// variable = 123;

// function sayName(variable){
// 	console.log("Hello " + variable);
// }

// sayName(variable); // Hello 123

function sayHello() {
	let name = "arvin";
	console.log(name);
}
