import React, { Fragment } from 'react';
import Course from '../components/Course';
import coursesData from '../data/courses';

export default function Courses() {

	const CourseCards = coursesData.map((course) => {
		return <Course key={course.id} course={course}/>;
	});

	return <Fragment>{CourseCards}</Fragment>

}

/*coursesData = [{"PHP-Laravel"},{"Python - Django"},{"Java Springboot"}]

course = {
	id: "wdc001",
	name: "PHP-Laravel",
	description: "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor",
	price: 45000,
	onOffer: true
}

const CourseCards = coursesData.map((course) => {
	return <Course key={course.id} course={course}/>
});

// CourseCards = 
// [
// 	<Course key="wdc001" course="{'PHP-Laravel'}" count="5"/>,
// 	<Course key="wdc002" course="{'Python - Django'}" count="3"/>,
// 	<Course key="wdc003" course="{'Java Springboot'}" count="10"/>,
// ]

// ES6 syntax for anonymous
() => {}*/