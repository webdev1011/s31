const coursesData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description:"Nostrud velit dolor excepteur ullamco consectetur aliquip tempor",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor",
		price: 55000,
		onOffer: true
	}
]

export default coursesData;