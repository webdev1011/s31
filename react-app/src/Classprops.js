import React, { Component } from 'react';

class Classprops extends Component {
	render(){
		return(
			<div>
				<h1>Hi! My name is {this.props.name}, I am {this.props.age} years old</h1>
			</div>
		)
	}
}

export default Classprops;