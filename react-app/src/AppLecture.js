import React from 'react';
import FC from './components/Function';
import { Class, ClassComp } from './components/Class';
import Classprops from './Classprops';

class App extends React.Component{
  render(){
    return (
        <div>
           <h1>Hello po!</h1>
           <Classprops name="Ken" age="24"/>
           <Classprops name="Janine" age="18" />
           <Classprops name="Jacob" age="20" />
           <Classprops name="Paolo" age="22" />
           <Classprops name="Sharmane" age="12"/>
           <h2>This is components</h2>
           <FC />
           <Class />
           <ClassComp />
        </div>
    );
  }
}
  

export default App;
