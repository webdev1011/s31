import React, { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import Navbar from './components/Navbar';

import Home from './pages/Home';
import Courses from './pages/Courses';



function App() {
	return (
		<Fragment>
			<Navbar />
			<Home />
			<Container>
				<br />
				<Courses />
			</Container>
		</Fragment>
	);
}

export default App;